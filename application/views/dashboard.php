<div class="boxed">

    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
      <div id="page-head">

        <!--Page Title-->
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div id="page-title">
            <h3>Sistem Informasi Kerja Praktik</h3>
            <h6>Selamat datang di aplikasi sistem informasi kerja praktik berbasis web  fakultas teknik elektro</h6>
        </div>

        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <!--End page title-->


        <!--Breadcrumb-->
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <ol class="breadcrumb">
        <li><a href="#"><i class="demo-pli-home"></i></a></li>
        <li><a href="#">Dashboard</a></li>
        </ol>
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <!--End breadcrumb-->

    </div>

    <!--Page content-->
    <!--===================================================-->
    <div id="page-content">
            
      <div class="panel">

          <div class="pad-all">

              <div id="demo-gallery" style="display:none;">
  
                  <a href="#">
                      <img alt="Telkom University"
                           src="<?php echo base_url('assets/img/caro/1.jpg');?>"
                           data-image=<?php echo base_url('assets/img/caro/1.jpg');?>"
                           data-description="Directory telkom university"
                           style="display:none">
                  </a>
  
                  <a href="#">
                      <img alt="Telkom University 2nd"
                           src="<?php echo base_url('assets/img/caro/2.jpg');?>"
                           data-image="<?php echo base_url('assets/img/caro/2.jpg');?>"
                           data-description="Aniversary Telkom University"
                           style="display:none">
                  </a>
  
                  <a href="#">
                      <img alt="Telkom Univrsity 3rd"
                           src="<?php echo base_url('assets/img/caro/3anv.jpg');?>"
                           data-image="<?php echo base_url('assets/img/caro/3anv.jpg');?>"
                           data-description="Aniversary Telkom University yang ke 3."
                           style="display:none">
                  </a>
  
                  <a href="#">
                      <img alt="Telkom University 4rd"
                           src="<?php echo base_url('assets/img/caro/4anv.png');?>"
                           data-image="<?php echo base_url('assets/img/caro/4anv.png');?>"
                           data-description="Aniversary Telkom yang ke 4"
                           style="display:none">
                  </a>
  
                  <a href="#">
                      <img alt="Telkom University 5th"
                           src="<?php echo base_url('assets/img/caro/5anv.png');?>"
                           data-image="<?php echo base_url('assets/img/caro/5anv.png');?>"
                           data-description="Aniversary Telkom yang ke 5"
                           style="display:none">
                  </a>
              
                  </div>
              </div>
          </div>

    </div>
    <!--===================================================-->
    <!--End page content-->


     