<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from www.themeon.net/nifty/v2.9.1/pages-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 13 Jul 2018 13:18:47 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Register page | Si.Kp </title>


    <!--STYLESHEET-->
    <!--=================================================-->

    <!--Open Sans Font [ OPTIONAL ]-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>


    <!--Bootstrap Stylesheet [ REQUIRED ]-->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">


    <!--Nifty Stylesheet [ REQUIRED ]-->
    <link href="<?php echo base_url('assets/css/nifty.min.css');?>" rel="stylesheet">


    <!--Nifty Premium Icon [ DEMONSTRATION ]-->
    <link href="<?php echo base_url('assets/css/demo/nifty-demo-icons.min.css');?> " rel="stylesheet">


    <!--=================================================-->



    <!--Pace - Page Load Progress Par [OPTIONAL]-->
    <link href="<?php echo base_url('assets/plugins/pace/pace.min.css');?>" rel="stylesheet">
    <script src="<?php echo base_url('assets/plugins/pace/pace.min.js');?>"></script>


        
    <!--Demo [ DEMONSTRATION ]-->
    <link href="<?php echo base_url('assets/css/demo/nifty-demo.min.css');?>" rel="stylesheet">

    
    <!--=================================================

    REQUIRED
    You must include this in your project.


    RECOMMENDED
    This category must be included but you may modify which plugins or components which should be included in your project.


    OPTIONAL
    Optional plugins. You may choose whether to include it in your project or not.


    DEMONSTRATION
    This is to be removed, used for demonstration purposes only. This category must not be included in your project.


    SAMPLE
    Some script samples which explain how to initialize plugins or components. This category should not be included in your project.


    Detailed information and more samples can be found in the document.

    =================================================-->
        
</head>

<!--TIPS-->
<!--You may remove all ID or Class names which contain "demo-", they are only used for demonstration. -->

<body>
     <div id="container" class="cls-container">
        
		
		<!-- BACKGROUND IMAGE -->
		<!--===================================================-->
		<div id="bg-overlay"></div>
		
		
		<!-- REGISTRATION FORM -->
		<!--===================================================-->
		<div class="cls-content">
		    <div class="cls-content-lg panel">
		        <div class="panel-body">
		            <div class="mar-ver pad-btm">
		                <h1 class="h3">Create a New Account</h1>
		                <p>Masukan data lengkap untuk menjadi pembimbing lapangan kerja praktik Universitas Telkom. </p>
		            </div>
		            <form action="*">
		                <div class="row">
		                    <div class="pad-btm">
		                        <div class="form-group" >
		                            <input type="text" class="form-control" placeholder="Nama lengkap" name="name">
		                        </div>
		                        <div class="form-group">
		                            <input type="text" class="form-control" placeholder="E-mail Instansi" name="email">
		                        </div>
		                        <div class="form-group">
		                            <input type="text" class="form-control" placeholder="Username" name="username">
		                        </div>
		                        <div class="form-group">
		                            <input type="password" class="form-control" placeholder="Password" name="password">
		                        </div>
		                        <div class="form-group">
		                            <input type="password" class="form-control" placeholder="Confrim Password" name="password2">
		                        </div>
		                        <div class="form-group">
    	                            <select class="btn btn-primary btn-block">
										<option>Status level akun...</option>
										<option value="1" type="enum" name="status" >Pembimbing Lapangan</option>
									</select>
		                    	</div>
		                    	<button class="btn btn-primary btn-lg btn-block" type="submit">Register</button>
		                    </div>
		                </div>
		                
		                
		            </form>
		        </div>
		        <div class="pad-all">
		            Already have an account ? <a href="<?php echo base_url('index.php');;?>" class="btn-link mar-rgt text-bold">Sign In</a>
		
		            
		        </div>
		    </div>
		</div>
		<!--===================================================-->
		
		
		
		
		
    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->


        
    <!--JAVASCRIPT-->
    <!--=================================================-->

    <!--jQuery [ REQUIRED ]-->
    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>


    <!--BootstrapJS [ RECOMMENDED ]-->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>


    <!--NiftyJS [ RECOMMENDED ]-->
    <script src="<?php echo base_url('assets/js/nifty.min.js');?>"></script>




    <!--=================================================-->
    
    <!--Background Image [ DEMONSTRATION ]-->
    <script src="<?php echo base_url('assets/js/demo/bg-images.js');?>"></script>

</body>
</html>
