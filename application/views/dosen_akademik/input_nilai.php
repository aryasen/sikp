<div class="boxed">
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
        <div id="page-head">

            <!--Page Title-->
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <div id="page-title">
                <h3>Sistem Informasi Kerja Praktik</h3>
                <h6>Aplikasi sistem informasi berbasis web yang bertujuan mempermudah kerja praktik fakultas teknik elektro</h6>
            </div>

            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <!--End page title-->


            <!--Breadcrumb-->
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <ol class="breadcrumb">
            <li><a href="#"><i class="demo-pli-home"></i></a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Input Nilai KP</li>
            </ol>
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <!--End breadcrumb-->
        </div>



        <div id="page-content">
            <div class="panel" style="background-color: #f9f9f9">
                <div class="panel-body">
                     <div class="form-group" align="center">
                        <label class="control-label"><h3>Input Nilai Mahasiswa Bimbingan</h3></label>
                    </div><div class="fixed-fluid">
                        <h3 class="panel-title"></h3>
                    </div>

                    <!-- Foo Table - Filtering -->
              <!--===================================================-->
              <div class="panel-body">
                  <table id="demo-foo-filtering" class="table table-bordered table-hover toggle-circle" data-page-size="7">
                      <thead>
                          <tr>
                              <th data-toggle="true" style="text-align: center;">NO</th>
                              <th style="text-align: center;"> NIM</th>
                              <th style="text-align: center;"> Kelas</th> 
                              <th data-hide="phone, tablet" style="text-align: center;">Program Studi</th>
                              <th data-hide="phone, tablet" style="text-align: center;">Nilai</th>
                              <th data-hide="phone, tablet" style="text-align: center;">Input Nilai</th>
                          </tr>
                      </thead>
                      <div class="pad-btm form-inline">
                          <div class="row">
                              <div class="col-sm-6 text-xs-center" hidden="">
                                  <div class="form-group">
                                      <label class="control-label">Progam Studi</label>
                                      <select id="demo-foo-filter-status" class="form-control">
                                          <option value="">Show all</option>
                                      
                                      </select>
                                  </div>
                              </div>
                              <div class="col-sm-12 text-xs-center text-right">
                                  <div class="form-group">
                                      <input id="demo-foo-search" type="text" placeholder="Search" class="form-control" autocomplete="off">
                                  </div>
                              </div>
                          </div>
                      </div>
                      <tbody>
                        <?php
                        $no = 1; 
                        foreach ($tb as $tb) { ?>
                        <tr>
                          <td style="text-align: right; padding: 8px 20px 8px;"><?php echo $no++; ?></td>
                          <td style="text-align: center;"><?php echo $tb['nim']; ?></td>
                          <td style="text-align: center;"><?php echo $tb['kelas']; ?></td>
                          <td style="text-align: center;"><?php echo $tb['nama_program_studi'];?></td>
                          <td style="text-align: center;"><?php echo $tb['nilai_dosena'];?></td>
                          <td style="text-align: center;"><button data-target="#form-nilaimodala" data-toggle="modal" data-uid_mhs="<?php echo $tb['uid_mahasiswa']; ?>" data-id="<?php echo $tb['uidform_nilai'];?>" class="btn btn-xs btn-mint btn-icon"><i class="ion-compose icon-sm"></i></button></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                      <tfoot>
                          <tr>
                              <td colspan="6">
                                  <div class="text-right">
                                      <ul class="pagination"></ul>
                                  </div>
                              </td>
                          </tr>
                      </tfoot>
                  </table>
              </div>
              <!--===================================================-->
              <!-- End Foo Table - Filtering -->

            </div> 
        </div>            
    </div>
</div>
</div>

    <!--Default Bootstrap Modal-->
    <!--===================================================-->
<form action="<?php echo base_url('index.php/dosena/input_nilai_dosena/idata_nilai');?>" method="POST" id="formeditnilaidosena">
    <div class="modal fade" id="form-nilaimodala" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!--Modal header-->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                    <h4 class="modal-title">INPUT NILAI MAHASISWA</h4>
                </div>

                        <!--Modal body-->
                       
                    <div class="modal-body">
                        <div class="row" >
                            <div class="form-group">
                                <label class="col-sm-3 control-label" >NIM</label>
                                <div class="col-sm-9">
                                  <p class="form-control-static" id="nimisimodal">nim</p>
                                  <input type="hidden" name="uid_mhs" id="uid_mhs">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" >NAMA</label>
                                <div class="col-sm-9">
                                  <p class="form-control-static" id="namaisimodal">nama</p>
                                  <input type="hidden" name="nama" id="nama">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="nilai_a" >Input Nilai (0-100)</label>
                                <div class="col-sm-9">
                                   <input type="number" min="0" max="100" name="nilai_a" placeholder=".input-sm" class="form-control input-sm" id="nilai_a">
                                </div>
                            </div>
                        </div>
                    </div>
                

                  <!--Modal footer-->
                  <div class="modal-footer">
                    <button data-bb-handler="cancel" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button data-bb-handler="confirm" type="Submit" class="btn btn-primary">OK</button>
                  </div>
                
            
        </div>
    </div>
</form>
    <!--===================================================-->
    <!--End Default Bootstrap Modal-->
