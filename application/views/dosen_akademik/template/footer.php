            </div>
            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">


                    <!--OPTIONAL : ADD YOUR LOGO TO THE NAVIGATION-->
                    <!--It will only appear on small screen devices.-->
                    <!--================================
                    <div class="mainnav-brand">
                        <a href="index.html" class="brand">
                            <img src="img/logo.png" alt="Nifty Logo" class="brand-icon">
                            <span class="brand-text">Nifty</span>
                        </a>
                        <a href="#" class="mainnav-toggle"><i class="pci-cross pci-circle icon-lg"></i></a>
                    </div>
                    -->



                    <!--Menu-->
                    <!--================================-->
                    <div id="mainnav-menu-wrap">
                        <div class="nano">
                            <div class="nano-content">

                                <!--Profile Widget-->
                                <!--================================-->
                                <div id="mainnav-profile" class="mainnav-profile">
                                    <div class="profile-wrap text-center">
                                        <div class="pad-btm">
                                            <img class="img-circle img-md" src="<?php echo base_url('assets/img/profile-photos/4.png');?>" alt="Profile Picture">
                                        </div>
                                        <a href="#profile-nav" class="box-block" data-toggle="collapse" aria-expanded="false">
                                            <span class="pull-right dropdown-toggle">
                                                <i class="dropdown-caret"></i>
                                            </span>
                                            <p class="mnp-name">Anda login sebagai <?php echo $_SESSION['fullname']; ?></p>
                                           
                                        </a>
                                    </div>
                                    <div id="profile-nav" class="collapse list-group bg-trans">
                                        <a href="#" class="list-group-item">
                                            <i class="demo-pli-male icon-lg icon-fw"></i> View Profile
                                        </a>
                                        <a href="#" class="list-group-item">
                                            <i class="demo-pli-gear icon-lg icon-fw"></i> Settings
                                        </a>
                                        <a href="#" class="list-group-item">
                                            <i class="demo-pli-information icon-lg icon-fw"></i> Help
                                        </a>
                                        <a href="<?php echo site_url('logout');;?>" class="list-group-item">
                                            <i class="demo-pli-unlock icon-lg icon-fw"></i> Logout
                                        </a>
                                    </div>
                                </div>


                                <!--Shortcut buttons-->
                                <!--================================-->
                                <div id="mainnav-shortcut" class="hidden">
                                    <ul class="list-unstyled shortcut-wrap">
                                        <li class="col-xs-3" data-content="My Profile">
                                            <a class="shortcut-grid" href="#">
                                                <div class="icon-wrap icon-wrap-sm icon-circle bg-mint">
                                                <i class="demo-pli-male"></i>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="col-xs-3" data-content="Messages">
                                            <a class="shortcut-grid" href="#">
                                                <div class="icon-wrap icon-wrap-sm icon-circle bg-warning">
                                                <i class="demo-pli-speech-bubble-3"></i>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="col-xs-3" data-content="Activity">
                                            <a class="shortcut-grid" href="#">
                                                <div class="icon-wrap icon-wrap-sm icon-circle bg-success">
                                                <i class="demo-pli-thunder"></i>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="col-xs-3" data-content="Lock Screen">
                                            <a class="shortcut-grid" href="#">
                                                <div class="icon-wrap icon-wrap-sm icon-circle bg-purple">
                                                <i class="demo-pli-lock-2"></i>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <!--================================-->
                                <!--End shortcut buttons-->


                                <ul id="mainnav-menu" class="list-group">
                        
                                   
                        
                                    <!--Menu list item-->
                                    <li class="active-sub">
                                        <a href="<?php echo base_url('index.php/dosena/Dashboard');?>">
                                            <i class="demo-pli-home"></i>
                                            <span class="menu-title">Dashboard</span>
                                        </a>
                                    </li>

                                    <!--Category name-->
                                    <li class="list-header">Navigation</li>
                                    <li>
                                         <a href="#">
                                            <i class="demo-pli-receipt-4"></i>
                                            <span class="menu-title">Menu Dosen Akademik</span>
                                            <i class="arrow"></i>
                                        </a>
                                        <!--Submenu-->
                                        <ul class="collapse in">
                                            <li class="active-link"><a href="<?php echo base_url('index.php/dosena/ACC_form_pelaporan');?>"><i class="ti-arrow-circle-right"></i>Cek proposal KP</a></li>

                                            <li class="list-divider"></li>
                                            <li class="active-link"><a href=""><i class="ti-arrow-circle-right"></i>Lihat data Laporan</a></li> 
                                            
                                        </ul>
                                    </li>
                        
                                    
                        
                        
                                    <!--Category name-->
                                    <li class="list-header">Input Data</li>
                        
                                    
                        
                                    <!--Menu list item-->
                                    <li>
                                        <a href="#">
                                            <i class="demo-pli-pen-5"></i>
                                            <span class="menu-title">Forms Nilai KP</span>
                                            <i class="arrow"></i>
                                        </a>
                        
                                        <!--Submenu-->
                                        <ul class="collapse">
                                            <li class="active-link"><a href="<?php echo base_url('index.php/dosena/input_nilai_dosena');?>"><i class="ti-arrow-circle-right"></i>Input nilai mahasiswa</a></li>
                                            
                                            
                                        </ul>
                                    </li>
                        
                                    
                        
                                    <li class="list-divider"></li>
                        
                                 </ul>   
                    
                            </div>
                        </div>
                    </div>
                    <!--================================-->
                    <!--End menu-->

                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

        </div>

        

        <!-- FOOTER -->
        <!--===================================================-->
        <footer id="footer">

             

            <p class="pad-rgt">&#0169; 2018 Sistem informasi Kerja Praktik </p>



        </footer>
        <!--===================================================-->
        <!-- END FOOTER -->


        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->
    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->


    
    
    
     <!--JAVASCRIPT-->
    <!--=================================================-->

    <!--jQuery [ REQUIRED ]-->
    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>


    <!--BootstrapJS [ RECOMMENDED ]-->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>


    <!--NiftyJS [ RECOMMENDED ]-->
    <script src="<?php echo base_url('assets/js/nifty.min.js');?>"></script>

    <!--=================================================-->
    
    <!--FooTable [ OPTIONAL ]-->
    <script src="<?php echo base_url('assets/plugins/fooTable/dist/footable.all.min.js');?>"></script>


    <!--FooTable Example [ SAMPLE ]-->
    <script src="<?php echo base_url('assets/js/demo/tables-footable.js');?>"></script>
    
    <!--Unite Gallery [ OPTIONAL ]-->
    <script src="<?php echo base_url('assets/plugins/unitegallery/js/unitegallery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/plugins/unitegallery/themes/carousel/ug-theme-carousel.js');?>"></script>
    
    <!--Custom script [ DEMONSTRATION ]-->
    <!--===================================================-->
    <script>
        $(document).on('nifty.ready', function () {
            
            
            $("#demo-gallery").unitegallery({
                tile_enable_shadow: false
            });
            
            
                
        });

        var site_url = "<?php echo site_url();?>";
        var base_url = "<?php echo base_url();?>";
    </script>

    <script src="<?php echo base_url('assets/js/dosena.js');?>"></script>
    

 </body>
</html>
