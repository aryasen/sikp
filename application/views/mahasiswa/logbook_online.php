<div class="boxed">

    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
        <div id="page-head">

            <!--Page Title-->
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <div id="page-title">
                <h3>Sistem Informasi Kerja Praktik</h3>
                <h6>Aplikasi sistem informasi berbasis web yang bertujuan mempermudah kerja praktik fakultas teknik elektro</h6>
            </div>

            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <!--End page title-->


            <!--Breadcrumb-->
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <ol class="breadcrumb">
            <li><a href="#"><i class="demo-pli-home"></i></a></li>
            <li><a href="#">Input Data Kegiatan KP</a></li>
            <li class="active">Logbook Online</li>
            </ol>
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <!--End breadcrumb-->
        </div>


		<div id="page-content">
            <div class="panel">
                <div class="panel-body">
                     <div class="form-group">
                        <label class="control-label"><h3>Logbook Online</h3></label>
                    </div>
                    <div class="fixed-fluid">
                        <h3 class="panel-title"></h3>
                	</div>

					
		            <!-- Bubble Numbers Form Wizard -->
		            <!--===================================================-->
		            <div id="demo-step-wz">
		                <div class="wz-heading wz-w-label bg-info ">
		
		                    <!--Progress bar-->
		                    <div class="progress progress-xs">
		                        <div style="width: 15%;" class="progress-bar progress-bar-dark"></div>
		                    </div>
		
		                    <!--Nav-->
		                    <ul class="wz-steps wz-icon-bw wz-nav-off text-lg">
		                        <li class="col-md-2">
		                            <a data-toggle="tab" href="#demo-step-tab1">
		                                <span class="icon-wrap icon-wrap-xs icon-circle bg-dark mar-ver">
		                                    <span class="wz-icon icon-txt text-bold">1</span>
		                                    <i class="wz-icon-done demo-psi-like"></i>
		                                </span>
		                                <small class="wz-desc box-block text-semibold">Senin</small>
		                            </a>
		                        </li>
		                        <li class="col-md-2">
		                            <a data-toggle="tab" href="#demo-step-tab2">
		                                <span class="icon-wrap icon-wrap-xs icon-circle bg-dark mar-ver">
		                                    <span class="wz-icon icon-txt text-bold">2</span>
		                                    <i class="wz-icon-done demo-psi-like"></i>
		                                </span>
		                                <small class="wz-desc box-block text-semibold">Selasa</small>
		                            </a>
		                        </li>
		                        <li class="col-md-2">
		                            <a data-toggle="tab" href="#demo-step-tab3">
		                                <span class="icon-wrap icon-wrap-xs icon-circle bg-dark mar-ver">
		                                    <span class="wz-icon icon-txt text-bold">3</span>
		                                    <i class="wz-icon-done demo-psi-like"></i>
		                                </span>
		                                <small class="wz-desc box-block text-semibold">Rabu</small>
		                            </a>
		                        </li>
		                        <li class="col-md-2">
		                            <a data-toggle="tab" href="#demo-step-tab4">
		                                <span class="icon-wrap icon-wrap-xs icon-circle bg-dark mar-ver">
		                                    <span class="wz-icon icon-txt text-bold">4</span>
		                                    <i class="wz-icon-done demo-psi-like"></i>
		                                </span>
		                                <small class="wz-desc box-block text-semibold">Kamis</small>
		                            </a>
		                        </li>
		                        <li class="col-md-2">
		                            <a data-toggle="tab" href="#demo-step-tab5">
		                                <span class="icon-wrap icon-wrap-xs icon-circle bg-dark mar-ver">
		                                    <span class="wz-icon icon-txt text-bold">5</span>
		                                    <i class="wz-icon-done demo-psi-like"></i>
		                                </span>
		                                <small class="wz-desc box-block text-semibold">Jumat</small>
		                            </a>
		                        </li>					              
		                    </ul>
		                </div>
					
		                <!--Form-->
		                <form class="form-horizontal">
		                    <div class="panel-body">
		                        <div class="tab-content">
		
		                            <!--First tab-->
		                            <div id="demo-step-tab1" class="tab-pane fade">
		                                <div class="form-group">
		                                    <label class="col-lg-3 control-label">Kegiatan senin</label>
		                                    <div class="col-lg-7">
		                                        <textarea id="demo-textarea-input" rows="9" class="form-control" placeholder="Your content here.."></textarea>
		                                    </div>
		                                </div>
		                            </div>
					
		                            <!--Second tab-->
		                            <div id="demo-step-tab2" class="tab-pane fade">
		                                <div class="form-group">
		                                    <label class="col-lg-3 control-label">Kegiatan selasa</label>
		                                    <div class="col-lg-7">
		                                        <textarea id="demo-textarea-input" rows="9" class="form-control" placeholder="Your content here.."></textarea>
		                                    </div>					        
		                            </div>
		
		                            <!--Third tab-->
		                            <div id="demo-step-tab3" class="tab-pane fade">
		                                <div class="form-group">
		                                    <label class="col-lg-3 control-label">Kegiatan rabu</label>
		                                    <div class="col-lg-7">
		                                       <textarea id="demo-textarea-input" rows="9" class="form-control" placeholder="Your content here.."></textarea>
		                                    </div>
		                                </div>					                                
		                            </div>
		
		                            <!--Fourth tab-->
		                            <div id="demo-step-tab4" class="tab-pane fade">
		                                <div class="form-group">
		                                    <label class="col-lg-3 control-label">Kegiatan kamis</label>
		                                    <div class="col-lg-7">
		                                        <textarea id="demo-textarea-input" rows="9" class="form-control" placeholder="Your content here.."></textarea>
		                                    </div>
		                                </div>
		                            </div>

		                            <!--Fifth tab-->
		                            <div id="demo-step-tab5" class="tab-pane fade">
		                                <div class="form-group">
		                                    <label class="col-lg-3 control-label">Kegiatan jumat</label>
		                                    <div class="col-lg-7">
		                                       <textarea id="demo-textarea-input" rows="9" class="form-control" placeholder="Your content here.."></textarea>
		                                    </div>
		                                </div>
		                            </div>


		                             <!--Footer button-->
				                    <div class="panel-footer text-right">
				                        <div class="box-inline">
				                            <button type="button" class="previous btn btn-info">Previous</button>
				                            <button type="button" class="next btn btn-info">Next</button>
				                            <button type="button" class="finish btn btn-info" disabled>Finish</button>
				                        </div>
				                    </div>

				                </div>
				            </div>
			            </form>				            
			            <!--===================================================-->
			            <!-- End Bubble Numbers Form Wizard -->
			        </div>
			    </div>       
			</div>           	
		</div>	            
	</div>
			
