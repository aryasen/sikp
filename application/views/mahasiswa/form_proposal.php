<div class="boxed">

    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
        <div id="page-head">

            <!--Page Title-->
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <div id="page-title">
                <h3>Sistem Informasi Kerja Praktik</h3>
                <h6>Aplikasi sistem informasi berbasis web yang bertujuan mempermudah kerja praktik fakultas teknik elektro</h6>
            </div>

            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <!--End page title-->


            <!--Breadcrumb-->
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <ol class="breadcrumb">
            <li><a href="#"><i class="demo-pli-home"></i></a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Form Peroposal KP</li>
            </ol>
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <!--End breadcrumb-->
        </div>



        <div id="page-content">
            <div class="panel">
                <div class="panel-body">
                    <div class="form-group" align="center">
                        <label class="control-label"><h3>FORM PROPOSAL KERJA PRAKTIK</h3></label>
                    </div>
                    <div class="fixed-fluid">
                        <h3 class="panel-title"></h3>
                    </div>


                    <!-- BASIC FORM ELEMENTS -->
                    <!--===================================================-->
                    <form class="panel-body form-horizontal form-padding" style="background-color:#dae3f2" form action="<?php echo base_url('index.php/mahasiswa/Form_proposal/idata_fproposal');?>" method="POST">

                        <!--Static-->
                        <div class="form-group">
                            <label class="control-label"><h5>IDENTITAS MAHASISWA :</h5></label>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nama</label>
                            <div class="col-md-9"><p class="form-control-static"><?php echo $_SESSION['fullname'] ?></p></div>
                            <input type="hidden" value="<?php echo $_SESSION['fullname']; ?>" name="inputFullname">
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">NIM</label>
                            <div class="col-md-9"><p class="form-control-static"><?php echo $_SESSION['nim'] ?></p></div>
                            <input type="hidden" value="<?php echo $_SESSION['nim']; ?>" name="inputNIM">
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Program Studi</label>
                            <div class="col-md-9"><p class="form-control-static"><?php echo $_SESSION['nama_program_studi'] ?></p></div>
                            <input type="hidden" value="<?php echo $_SESSION['nama_program_studi']; ?>" name="inputnama_program_studi">
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nomer Kontak</label>
                            <div class="col-md-9"><p class="form-control-static"><?php echo $_SESSION['no_kontak'] ?></p></div>
                            <input type="hidden" value="<?php echo $_SESSION['no_kontak']; ?>" nama="inputnp_kontak">
                        </div>
                        <div class="form-group" >
                            <label class="col-md-3 control-label" for="demo-text-input">Email</label>
                            <div class="col-md-9">
                                <input type="email" id="demo-text-input" class="form-control" name="email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="demo-text-input">Pengajuan ke</label>
                            <div class="col-md-9">
                                <input type="text" id="demo-text-input" class="form-control" name="pengajuanke">
                            </div>
                        </div>
                        <!--Textarea-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="demo-textarea-input">Alasan Pengajuan Ulang</label>
                            <div class="col-md-9">
                                <textarea id="demo-textarea-input" rows="9" class="form-control" placeholder="Your content here.." name="apu"></textarea>
                                <small><i>(diisi bagi mahasiswa yang sudah pernah mengajukan permohoonan sebelumnya)</i></small>
                            </div>
                        </div>



                        <!--Input dta kantor-->
                        <div class="form-group">
                            <label class="control-label"><h5>IDENTITAS PERUSAHAAN :</h5></small></label>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="demo-text-input">Nama Lengkap Perusahaan</label>
                            <div class="col-md-9">
                                <input type="text" id="demo-text-input" class="form-control" name="nli">      
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="demo-text-input">Alamat Lengkap Perusahaan</label>
                            <div class="col-md-9">
                                <input type="text" id="demo-text-input" class="form-control" name="ali">      
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="demo-text-input">Kota</label>
                            <div class="col-md-9">
                                <input type="text" id="demo-text-input" class="form-control" name="kota">      
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="demo-text-input">Kode pos</label>
                            <div class="col-md-9">
                                <input type="number" id="demo-text-input" class="form-control" name="kodepos" min="0">      
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="demo-text-input">Surat di tujukan kepada(nama lengkap)</label>
                            <div class="col-md-9">
                                <input type="text" id="demo-text-input" class="form-control" name="fullname">      
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="demo-text-input">Devisi/Bagian</label>
                            <div class="col-md-9">
                                <input type="text" id="demo-text-input" class="form-control" name="devb">      
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="demo-text-input">Posisi/Jabatan</label>
                            <div class="col-md-9">
                                <input type="text" id="demo-text-input" class="form-control" name="posj">      
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="demo-text-input">No. Telpon/Fax Perusahaan</label>
                            <div class="col-md-9">
                                <input type="text" id="demo-text-input" class="form-control" name="notlp_i">      
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="demo-email-input">Email Perusahaan</label>
                            <div class="col-md-9">
                                <input type="email" id="demo-email-input" class="form-control" name="email_i">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="demo-text-input"><i>Contact Person</i> Perusahaan</label>
                            <div class="col-md-9">
                                <input type="number" id="demo-text-input" class="form-control" name="cpi" min="0">      
                            </div>
                        </div>
                        <div class="form-group input-daterange">
                            <label class="control-label col-md-3">Kegiatan kerja praktik di laksanakan tanggal</label>
                            <div class="col-md-9">
                                <div class="col-md-9 input-group">
                                    <input type="text" class="form-control" name="startkp" />
                                    <span class="input-group-addon">s/d</span>
                                    <input type="text" class="form-control" name="endkp" />
                                </div>
                            </div>
                        </div>

                        <!--bab1,2proposal-->                 
                       <div class="form-group">
                            <label class="control-label"><i><b>Masukan Proposal Rencana Kegiatan KP yang akan Saudara lakukan seperti di bawah ini</b></i></label>
                           <p>Proposal Rencana Kegiatan KP berisi 2(dua) Bab: </p>
                           <br>
                           <label class="control-label"><b>Bab I. Pendahuluan yang berisi minimal tentang:</b></label>
                           <p>(a) Latar belakang, berisi deskripso singkat tentang alasan mengapa Saudara akan melakukan Kerja Praktik di Instansi yang Saudara tuju;</p>
                           <p>(b) Tujuan kegiatan KP, berisi deskripsi singkat tentang tujuan kegiatan KP di instansi</p>
                           <p>(c) Manfaat KP, berisi deskripsi singkat tentang manfaat bagi mahasiswa dan bagi instansi yang Saudara tuju bila Saudara melakukan KP di Instansi</p>
                        </div>
                        
                        <!--Textarea-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="demo-textarea-input">Bab I. Pendahuluan</label>
                            <div class="col-md-9">
                                <textarea id="demo-textarea-input" rows="9" class="form-control" placeholder="Your content here.." name="BAB1"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label"><b>Bab II. Rencana Kegiatan KP:</b></label>
                               <p>(a) Rencana lama waktu yang di perlukan KP di instansi tujuan;</p>
                               <p>(b) Rencana kegiatan KP.</p>
                         </div>   
                        <!--Textarea-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="demo-textarea-input">Bab II. Rencana Kegiatan KP</label>
                            <div class="col-md-9">
                                <textarea id="demo-textarea-input" rows="9" class="form-control" placeholder="Your content here.." name="BAB2"></textarea>
                            </div>
                        </div>

                        <div class="form-group col-md-9 ">
                            <input class="btn btn-primary rollin" type="button" value="Submit proposal" data-target="#konfirmasi" data-toggle="modal"></input>
                       </div>

                    
                        <div class="modal fade" id="konfirmasi" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal" style="margin-top: -10px;">
                                        <i class="pci-cross pci-circle"></i>
                                    </button>
                                    <div class="bootbox-body">Apakah anda yakin semua data sudah benar?</div>
                                </div>
                                <div class="modal-footer">
                                    <button data-bb-handler="cancel" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <button data-bb-handler="confirm" type="Submit" class="btn btn-primary">OK</button>
                                </div>
                            </div>
                        </div>  
                    </form>

                </div>
            <div>
        </div>        
    </div>
</div>