<div class="boxed">
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
        <div id="page-head">

            <!--Page Title-->
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <div id="page-title">
                <h3>Sistem Informasi Kerja Praktik</h3>
                <h6>Aplikasi sistem informasi berbasis web yang bertujuan mempermudah kerja praktik fakultas teknik elektro</h6>
            </div>

            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <!--End page title-->


            <!--Breadcrumb-->
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <ol class="breadcrumb">
            <li><a href="#"><i class="demo-pli-home"></i></a></li>
            <li><a href="#">Navigation</a></li>
            <li class="active">Proposal KP</li>
            </ol>
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <!--End breadcrumb-->
        </div>



        <div id="page-content">
            <div class="panel" style="background-color: #f9f9f9">
                <div class="panel-body">
                     <div class="form-group" align="center">
                        <label class="control-label"><h3>FORM PERMOHONAN KERJA PRAKTIK</h3></label>
                    </div><div class="fixed-fluid">
                        <h3 class="panel-title"></h3>
                    </div>

                    <!-- Foo Table - Filtering -->
              <!--===================================================-->
              <div class="panel-body">
                  <table id="demo-foo-filtering" class="table table-bordered table-hover toggle-circle" data-page-size="7">
                      <thead>
                          <tr>
                              <th data-toggle="true" style="text-align: center;">NO</th>
                              <th style="text-align: center;"> NIM</th>
                              <th style="text-align: center;"> Program Studi</th>                            
                              <th data-hide="phone, tablet" style="text-align: center;">Perusahaan yang di tuju</th>
                              <th data-hide="phone, tablet" style="text-align: center;">Status</th>                             
                              <th data-hide="phone, tablet" style="text-align: center;">Action</th>
                          </tr>
                      </thead>
                      <div class="pad-btm form-inline">
                          <div class="row">
                              <div class="col-sm-6 text-xs-center">
                                  <div class="form-group">
                                      <label class="control-label">Status</label>
                                      <select id="demo-foo-filter-status" class="form-control">
                                          <option value="">Show all</option>
                                          <option value="belum">Belum </option>
                                          <option value="diterima">Diterima</option>
                                          <option value="ditolak">Ditolak</option>
                                          

                                      </select>
                                  </div>
                              </div>
                              <div class="col-sm-6 text-xs-center text-right">
                                  <div class="form-group">
                                      <input id="demo-foo-search" type="text" placeholder="Search" class="form-control" autocomplete="off">
                                  </div>
                              </div>
                          </div>
                      </div>
                      <tbody>
                        <?php
                        $no = 1; 
                        foreach ($tb as $tb) { ?>
                        <tr>
                          <td style="text-align: right; padding: 8px 20px 8px;"><?php echo $no++; ?></td>
                          <td style="text-align: center;"><?php echo $tb['nim']; ?></td>
                          <td style="text-align: center;"><?php echo $tb['nama_program_studi'];?></td>
                          <td style="padding: 8px 20px 8px;"><?php echo $tb['nama_lngp_instansi'];?></td>
                          <td style="text-align: center;">
                            <?php 
                            if($tb['konfirmasi_dosena']=="mhs_input"){
                              echo '<span class="label label-table label-primary">Belum</span>';
                            }else if($tb['konfirmasi_dosena']=="Diterima"){
                              echo '<span class="label label-table label-success">Diterima</span>';
                            }else if($tb['konfirmasi_dosena']=="Ditolak"){
                              echo '<span class="label label-table label-danger">Ditolak</span>';}
                            ?>
                          </td>
                          <td style="text-align: center;"><button data-target="#form-proposal-modal-mhs" data-toggle="modal" data-id="<?php echo $tb['uidform_proposal'];?>" class="btn btn-xs btn-mint btn-icon"><i class="ion-compose icon-sm"></i></button></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                      <tfoot>
                          <tr>
                              <td colspan="5">
                                  <div class="text-right">
                                      <ul class="pagination"></ul>
                                  </div>
                              </td>
                          </tr>
                      </tfoot>
                  </table>
              </div>
              <!--===================================================-->
              <!-- End Foo Table - Filtering -->

                    </div> 
            </div>            
        </div>
    
</div>


    <!--Default Bootstrap Modal-->
    <!--===================================================-->
    <div class="modal fade" id="form-proposal-modal-mhs" role="dialog" tabindex="-1" aria-labelledby="form-proposal-modal-admin" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!--Modal header-->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                    <h4 class="modal-title">Form Proposal</h4>
                </div>

                <!--Modal body-->
                <div class="modal-body">
                    <table class="table table-striped" >
                          <!-- masukin list form -->
                          <tr style="background-color:#dae3f2">
                            <td class="col-md-3" align="center">IDENTITAS MAHASISWA</td>
                            <td class="col-md-9">Data</td>
                          </tr>
                           <tr>
                            <td >Nim :</td>
                            <td id="tb_nim"></td>
                          </tr>
                          <tr>
                            <td>Nama :</td>
                            <td id="tb_fullname"> </td>
                          </tr>
                          <tr>
                            <td>Program Studi :</td>
                            <td id="tb_nama_program_studi"> </td>
                          </tr>
                          <tr>
                            <td>Nomer Kontak :</td>
                            <td id="tb_no_kontak"> </td>
                          </tr>
                          <tr>
                            <td>E-mail :</td>
                            <td id="tb_email"> </td>
                          </tr>
                           <tr>
                            <td>Pengajuan ke :</td>
                            <td id="tb_pengajuanke"> </td>
                          </tr>
                          <tr>
                            <td>Alasan pengajuan ulang :</td>
                            <td id="tb_alasan_pengajuan_ulang"> </td>
                          </tr>
                          <tr style="background-color:#dae3f2">
                            <td class="col-md-3" align="center">IDENTITAS PERUSAHAAN</td>
                            <td class="col-md-9">Data</td>
                          </tr>
                          <tr>
                            <td>Nama lengkap instasni :</td>
                            <td id="tb_nama_lngp_instansi"> </td>
                          </tr>
                          <tr>
                            <td>Alamat lengkap instansi :</td>
                            <td id="tb_alamat_lngp_instansi"> </td>
                          </tr>
                          <tr>
                            <td>Kota :</td>
                            <td id="tb_kota"> </td>
                          </tr>
                          <tr>
                            <td>Kode pos :</td>
                            <td id="tb_kodepos"> </td>
                          </tr>
                          <tr>
                            <td>Devisi/bagian :</td>
                            <td id="tb_devisi_bagian"> </td>
                          </tr>
                          <tr>
                            <td>Posisi/jabatan :</td>
                            <td id="tb_posisi_jabatan"> </td>
                          </tr>
                          <tr>
                            <td>Nomer telepon instansi :</td>
                            <td id="tb_notlp_instansi"> </td>
                          </tr>
                          <tr>
                            <td>E-mail instansi :</td>
                            <td id="tb_email_instansi"> </td>
                          </tr>
                          <tr>
                            <td>Contact person instansi :</td>
                            <td id="tb_contact_person_instansi"> </td>
                          </tr>
                          <tr>
                            <td>Tgl mulai :</td>
                            <td id="tb_tgl_mulai"> </td>
                          </tr>
                          <tr>
                            <td>Tgl selesai :</td>
                            <td id="tb_tgl_selesai"> </td>
                          </tr>
                          <tr style="background-color:#dae3f2">
                            <td class="col-md-3" align="center">RINGKASAN PROPOSAL</td>
                            <td class="col-md-9">Data</td>
                          </tr>
                          <tr>
                            <td>BAB 1 :</td>
                            <td id="tb_BAB1"> </td>
                          </tr>
                          <tr>
                            <td>BAB 2 :</td>
                            <td id="tb_BAB2"> </td>
                          </tr>
                    </table>
                </div>

                <!--Modal footer-->
                <div class="modal-footer">                    
                    <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                </div>
            </div>
        </div>
    
    <!--===================================================-->
    <!--End Default Bootstrap Modal-->
