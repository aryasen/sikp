<div class="boxed">

    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
	     <div id="page-head">

	        <!--Page Title-->
	        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	        <div id="page-title">
	            <h3>Sistem Informasi Kerja Praktik</h3>
	            <h6>Aplikasi sistem informasi berbasis web yang bertujuan mempermudah kerja praktik fakultas teknik elektro</h6>
	        </div>

	        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	        <!--End page title-->


	        <!--Breadcrumb-->
	        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	        <ol class="breadcrumb">
	        <li><a href="#"><i class="demo-pli-home"></i></a></li>
	        <li><a href="#">Dashboard</a></li>
	        <li class="active">Pelaporan KP</li>

	        </ol>
	        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	        <!--End breadcrumb-->
	    </div>


				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
				    
					<div class="panel">
					    <div class="panel-heading">
					        <h3 class="panel-title">UPLOAD DOKUMEN LAPORAN</h3>
					    </div>
						    <div class="panel-body">
						        <p class="text-main text-bold mar-no">File yang di upload berupa file Pdf lengkap dengan sub-bab nya</p>
						        <p>Harap cek kembali Data anda sebelum upload data</p>
						
						        <br>
						
						          <!--Dropzonejs-->
						        <!--===================================================-->
						        <form id="demo-dropzone" action="#" class="dropzone">
						            <div class="dz-default dz-message">
						                <div class="dz-icon">
						                    <i class="demo-pli-upload-to-cloud icon-5x"></i>
						                </div>
						                <div>
						                    <span class="dz-text">Drop files to upload</span>
						                    <p class="text-sm text-muted">or click to pick manually</p>
						                </div>
						            </div>
						            <div class="fallback">
						                <input name="file" type="file" multiple>
						            </div>
						        </form>
						        <!--===================================================-->
						        <!-- End Dropzonejs -->
					   		</div>
					</div>
					
					<div class="panel">
					    <div class="panel-heading">
					        <h3 class="panel-title">Bootstrap theme</h3>
					    </div>
					    <div class="panel-body">
					
					        <!--Dropzonejs using Bootstrap theme-->
					        <!--===================================================-->
					        <p>This is a bootstrap theme of Dropzone.js with a completely different user experience.</p>
					
					        <div class="bord-top pad-ver">
					            <!-- The fileinput-button span is used to style the file input field as button -->
					            <span class="btn btn-success fileinput-button dz-clickable">
					                <i class="fa fa-plus"></i>
					                <span>Add files...</span>
					            </span>
					
					            <div class="btn-group pull-right">
					                <button id="dz-upload-btn" class="btn btn-primary" type="submit" disabled>
					                    <i class="fa fa-upload-cloud"></i> Upload
					                </button>
					                <button id="dz-remove-btn" class="btn btn-danger cancel" type="reset" disabled>
					                    <i class="demo-psi-trash"></i>
					                </button>
					            </div>
					        </div>
					        <!--===================================================-->
					        <!--End Dropzonejs using Bootstrap theme-->
					    </div>
					


			<script>
		    </script>
	</div>
<!--===================================================-->
<!--End page content-->

</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->