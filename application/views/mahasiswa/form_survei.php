<div class="boxed">

    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
        <div id="page-head">

            <!--Page Title-->
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <div id="page-title">
                <h3>Sistem Informasi Kerja Praktik</h3>
                <h6>Aplikasi sistem informasi berbasis web yang bertujuan mempermudah kerja praktik fakultas teknik elektro</h6>
            </div>

            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <!--End page title-->


            <!--Breadcrumb-->
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <ol class="breadcrumb">
            <li><a href="#"><i class="demo-pli-home"></i></a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Form Survei</li>
            </ol>
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <!--End breadcrumb-->
        </div>

        <div id="page-content">
            <div class="panel">
                <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label"><h3>FORM BERITA ACARA SURVEI/PENJAJAKAN LOKASI KERJA PRAKTIK</h3></label>
                    </div>
                    <div class="fixed-fluid">
                        <h3 class="panel-title"></h3>
                    </div>
                    
                    <!-- BASIC FORM ELEMENTS -->
                    <!--===================================================-->
                    <form class="panel-body form-horizontal form-padding" style="background-color:#dae3f2">

                        <!--Static-->
                         <div class="form-group">
                            <label class="control-label"><h5>IDENTITAS MAHASISWA :</h5></label>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nama</label>
                            <div class="col-md-9"><p class="form-control-static"><?php echo $_SESSION['fullname'] ?></p></div>
                            <input type="hidden" value="<?php echo $_SESSION['fullname']; ?>" name="inputFullname">
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">NIM</label>
                            <div class="col-md-9"><p class="form-control-static"><?php echo $_SESSION['nim'] ?></p></div>
                            <input type="hidden" value="<?php echo $_SESSION['nim']; ?>" name="inputNIM">
                        </div>
                        <!--Input dta kantor-->
                        <div class="form-group">
                            <label class="control-label"><h5>Telah melaksanakan survei/Penjajakan Lokasi KP di</h5></label>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="demo-text-input">Nama Perusahaan</label>
                            <div class="col-md-9">
                                <input type="text" id="demo-text-input" class="form-control">
                                
                            </div>
                        </div>

                         <div class="form-group">
                            <label class="col-md-3 control-label" for="demo-text-input">Alamat</label>
                            <div class="col-md-9">
                                <input type="text" id="demo-text-input" class="form-control" >
                                
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="demo-email-input">Email kantor/instansi</label>
                            <div class="col-md-9">
                                <input type="email" id="demo-email-input" class="form-control">
                            </div>
                        </div>

                        <!--Survei-->
                         <div class="form-group">
                            <label class="control-label"><h5>Survei/Penjajakan dilaksanakan dalam bentuk wawancara dengan perwakilan perusahaan</h5></label>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="demo-password-input">Nama</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control">
                            </div>
                        </div>

                        
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="demo-readonly-input">Bagian / Devisi</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="demo-readonly-input">Jabatan</label>
                            <div class="col-md-9">
                               <input type="text" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="demo-readonly-input">No. kontak</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="demo-readonly-input">E-mail Pribadi</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control">
                            </div>
                        </div>

                        <!--Ringkasan survei-->
                         <div class="form-group">
                            <label class="control-label"><h5>Dengan ringkasan Hasil Survei/Penjajakan sebagai berikut</h5></label>
                        </div>

           
                                            
                       <div class="form-group input-daterange">
                            <label class="control-label col-md-3">Kegiatan kerja praktik di laksanakan tanggal</label>
                            <div class="col-md-9">
                                <div class="col-md-9 input-group">
                                    <input type="text" class="form-control" name="start" />
                                    <span class="input-group-addon">to</span>
                                    <input type="text" class="form-control" name="end" />
                                </div>
                            </div>
                        </div>



                        <!--Textarea-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="demo-textarea-input">Keiatan/Aktivitas/Proyek yang dapat di kerjakan semalma pelaksanaan KP di perusahaan</label>
                            <div class="col-md-9">
                                <textarea id="demo-textarea-input" rows="9" class="form-control" placeholder="Your content here.."></textarea>
                            </div>
                        </div>

                       <div class="form-group col-md-9 ">
                            <input class="btn btn-primary rollin" type="button" value="Submit proposal" data-target="#konfirmasi_survei" data-toggle="modal"></input>
                       </div>
                        
                        
                        <div class="modal fade" id="konfirmasi_survei" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal" style="margin-top: -10px;">
                                        <i class="pci-cross pci-circle"></i>
                                    </button>
                                    <div class="bootbox-body">Apakah anda yakin semua data sudah benar?</div>
                                </div>
                                <div class="modal-footer">
                                    <button data-bb-handler="cancel" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <button data-bb-handler="confirm" type="Submit" class="btn btn-primary">OK</button>
                                </div>
                            </div>
                        </div>  
                    </form>
                </div>                        
           </div>
        </div>

