        </div>
            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">
                    <!--Menu-->
                    <!--================================-->
                    <div id="mainnav-menu-wrap">
                        <div class="nano">
                            <div class="nano-content">

                                <!--Profile Widget-->
                                <!--================================-->
                                <div id="mainnav-profile" class="mainnav-profile">
                                    <div class="profile-wrap text-center">
                                        <div class="pad-btm">
                                            <img class="img-circle img-md" src="<?php echo base_url('assets/img/profile-photos/1.png');?>" alt="Profile Picture">
                                        </div>
                                        <a href="#profile-nav" class="box-block" data-toggle="collapse" aria-expanded="false">
                                            <span class="pull-right dropdown-toggle">
                                                <i class="dropdown-caret"></i>
                                            </span>
                                            <p class="mnp-name">Anda login sebagai <?php echo $_SESSION['fullname']; ?></p>
                                           
                                        </a>
                                    </div>
                                    <div id="profile-nav" class="collapse list-group bg-trans">
                                        <a href="#" class="list-group-item">
                                            <i class="demo-pli-male icon-lg icon-fw"></i> View Profile
                                        </a>
                                        <a href="#" class="list-group-item">
                                            <i class="demo-pli-gear icon-lg icon-fw"></i> Settings
                                        </a>
                                        <a href="#" class="list-group-item">
                                            <i class="demo-pli-information icon-lg icon-fw"></i> Help
                                        </a>
                                        <a href="<?php echo site_url('logout');?>" class="list-group-item">
                                            <i class="demo-pli-unlock icon-lg icon-fw"></i> Logout
                                        </a>
                                    </div>
                                </div>


                                <!--Shortcut buttons-->
                                <!--================================-->
                                <div id="mainnav-shortcut" class="hidden">
                                    <ul class="list-unstyled shortcut-wrap">
                                        <li class="col-xs-3" data-content="My Profile">
                                            <a class="shortcut-grid" href="#">
                                                <div class="icon-wrap icon-wrap-sm icon-circle bg-mint">
                                                <i class="demo-pli-male"></i>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="col-xs-3" data-content="Messages">
                                            <a class="shortcut-grid" href="#">
                                                <div class="icon-wrap icon-wrap-sm icon-circle bg-warning">
                                                <i class="demo-pli-speech-bubble-3"></i>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="col-xs-3" data-content="Activity">
                                            <a class="shortcut-grid" href="#">
                                                <div class="icon-wrap icon-wrap-sm icon-circle bg-success">
                                                <i class="demo-pli-thunder"></i>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="col-xs-3" data-content="Lock Screen">
                                            <a class="shortcut-grid" href="#">
                                                <div class="icon-wrap icon-wrap-sm icon-circle bg-purple">
                                                <i class="demo-pli-lock-2"></i>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <!--================================-->
                                <!--End shortcut buttons-->


                                <ul id="mainnav-menu" class="list-group">                        
                                    <!--Menu list item-->
                                    <li class="active-sub">
                                        <a href="<?php echo base_url('index.php/mahasiswa/Dashboard');?>">
                                            <i class="demo-pli-home"></i>
                                            <span class="menu-title">Dashboard</span>
                                        </a>
                                    </li>
                                    <!--Category name-->
                                    <li class="list-header">Navigation</li>
                                    <li>
                                        <a>
                                            <i class="demo-pli-receipt-4"></i>
                                            <span class="menu-title">Menu Mahasiswa</span>
                                            <i class="arrow"></i>
                                        </a>
                                       <!--Submenu-->
                                        <ul class="collapse in">
                                            <li class="active-link"><a href="<?php echo base_url('index.php/mahasiswa/Form_survei');?>"><i class="ti-arrow-circle-right"></i>Form Survei</a></li>
                                            <li class="list-divider"></li>

                                            <li class="active-link"><a href="<?php echo base_url('index.php/mahasiswa/Form_proposal');?>"><i class="ti-arrow-circle-right"></i>Form Proposal KP</a></li>
                                            <li class="list-divider"></li>

                                            <li class="active-link"><a href="<?php echo base_url('index.php/mahasiswa/pelaporan_KP');?>"><i class="ti-arrow-circle-right"></i>Pelaporan KP</a></li>
                                        </ul>
                                    </li>
                                    
                                    <li class="list-divider"></li>
                                    <!--Category name-->
                                    <li class="list-header">Input Data</li>
                                    <!--Menu list item-->
                                    <li>
                                        <a href="#">
                                            <i class="demo-pli-pen-5"></i>
                                            <span class="menu-title">Input data kegiatan KP</span>
                                            <i class="arrow"></i>
                                        </a>
                                        <!--Submenu-->
                                        <ul class="collapse">
                                            <li class="active-link"><a href="<?php echo base_url('index.php/mahasiswa/logbook_online');?>"><i class="ti-arrow-circle-right"></i>Logbook online</a></li>   
                                        </ul>
                                    </li>
                                    <li class="list-divider"></li>
                                    <!-- end Menu list item-->
                                    
                                    <!--Category name-->
                                    <li class="list-header">Data Mahasiswa</li>
                                    <!--Menu list item-->
                                    <li>
                                        <a href="#">
                                            <i class="demo-pli-split-vertical-2"></i>
                                            <span class="menu-title">Lihat data Mahasiswa</span>
                                            <i class="arrow"></i>
                                        </a>
                                        <!--Submenu-->
                                        <ul class="collapse">
                                            <li class="active-link"><a href="<?php echo base_url('index.php/mahasiswa/di_tolak');?>""><i class="ti-arrow-circle-right"></i>Cek Proposal</a></li> 
                                        </ul>
                                        <ul class="collapse">
                                            <li class="active-link"><a href="#"><i class="ti-arrow-circle-right"></i>Cek Nilai</a></li>   
                                        </ul>
                                    </li>

                                    <li class="list-divider"></li>
                                    <!-- end Menu list item-->
                                </ul> 

                            </div>
                        </div>
                    </div>
                    <!--================================-->
                    <!--End menu-->
                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

        </div>
            <!-- FOOTER -->
            <!--===================================================-->
            <footer id="footer">
                <p class="pad-rgt">&#0169; 2018 Sistem informasi Kerja Praktik </p>
            </footer>
            <!--===================================================-->
            <!-- END FOOTER -->
            <!-- SCROLL PAGE BUTTON -->
            <!--===================================================-->
            <button class="scroll-top btn">
                <i class="pci-chevron chevron-up"></i>
            </button>
            <!--===================================================-->
        </div>
        <!--===================================================-->
        <!-- END OF CONTAINER -->




    
    
    
    <!--JAVASCRIPT-->
    <!--=================================================-->

        <!--jQuery [ REQUIRED ]-->
        <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>

        <!--BootstrapJS [ RECOMMENDED ]-->
        <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>

        <!--NiftyJS [ RECOMMENDED ]-->
        <script src="<?php echo base_url('assets/js/nifty.min.js');?>"></script>

        <!--Dropzone [ OPTIONAL ]-->
        <script src="<?php echo base_url('assets/plugins/dropzone/dropzone.min.js');?>"></script>

        <!--Form File Upload [ SAMPLE ]-->
        <script src="<?php echo base_url('assets/js/demo/form-file-upload.js');?>"></script>
        
         <!--Bootstrap Wizard [ OPTIONAL ]-->
        <script src="<?php echo base_url('assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js');?>"></script>

        <!--Bootstrap Validator [ OPTIONAL ]-->
        <script src="<?php echo base_url('assets/plugins/bootstrap-validator/bootstrapValidator.min.js');?>"></script>

        <!--Form Wizard [ SAMPLE ]-->
        <script src="<?php echo base_url('assets/js/demo/form-wizard.js');?>"></script>

        <!--Bootstrap Datepicker [ OPTIONAL ]-->
        <script src="<?php echo base_url('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js');?>"></script>

        <!--Demo script [ DEMONSTRATION ]-->
        <script src="<?php echo base_url('assets/js/demo/nifty-demo.min.js');?>"></script>
        
        <!--Unite Gallery [ OPTIONAL ]-->
        <script src="<?php echo base_url('assets/plugins/unitegallery/js/unitegallery.min.js');?>"></script>
        <script src="<?php echo base_url('assets/plugins/unitegallery/themes/carousel/ug-theme-carousel.js');?>"></script>
        
        <!--Icons [ SAMPLE ]-->
        <script src="<?php echo base_url('assets/js/demo/icons.js');?>"></script>

        <!--FooTable [ OPTIONAL ]-->
        <script src="<?php echo base_url('assets/plugins/fooTable/dist/footable.all.min.js');?>"></script>


        <!--FooTable Example [ SAMPLE ]-->
        <script src="<?php echo base_url('assets/js/demo/tables-footable.js');?>"></script>

       
    <!--=================================================-->
   

    <!--Custom script [ DEMONSTRATION ]-->
    <!--===================================================-->
        <script>
            $(document).on('nifty.ready', function () {
                
                
                $("#demo-gallery").unitegallery({
                    tile_enable_shadow: false
                });
                
                
                    
            });
            var site_url = "<?php echo site_url();?>";
            var base_url = "<?php echo base_url();?>";

            $('.input-daterange').datepicker({
                format: "mm, dd, yyyy",
                todayBtn: "linked",
                autoclose: true,
                todayHighlight: true
            });

          
        </script>
    <!--===================================================-->


    <script src="<?php echo base_url('assets/js/dosena.js');?>"></script>
    

 </body>
</html>
