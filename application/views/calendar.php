
<div class="boxed">
            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">
                <div id="page-head">
                    <!--Page Title-->
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <div id="page-title">
                        <h3>Sistem Informasi Kerja Praktik</h3>
                        <h6>Selamat datang di aplikasi sistem informasi kerja praktik berbasis web  fakultas teknik elektro</h6>
                    </div>

                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <!--End page title-->


                    <!--Breadcrumb-->
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="demo-pli-home"></i></a></li>
                        <li><a href="#">App Views</a></li>
                        <li class="active">Jadwal UPKP</li>
                    </ol>
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <!--End breadcrumb-->

                </div>

                
                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
                    
                    
                        <div class="panel">
                            <div class="panel-body">
                                <div class="fixed-fluid">
                                    <div class="fixed-sm-200 pull-sm-left fixed-right-border">
                                        <div class="form-group">
                                            <input type="text" id="event_title" placeholder="Event Title..." class="form-control" value="">
                                        </div>
                                        <button class="btn btn-block btn-purple btn-lg">Add New Event</button>
                                        <hr>
                    
                                        <!-- Draggable Events -->
                                        <!-- ============================================ -->
                                        <p class="text-muted text-sm text-uppercase">Draggable Events</p>
                                        <div id="demo-external-events">
                                            <div class="fc-event fc-list" data-class="warning">All Day Event</div>
                                            <div class="fc-event fc-list" data-class="success">Meeting</div>
                                            <div class="fc-event fc-list" data-class="mint">Birthday Party</div>
                                            <div class="fc-event fc-list" data-class="purple">Happy Hour</div>
                                            <div class="fc-event fc-list">Lunch</div>
                                            <hr>
                                            <div class="checkbox pad-btm text-left">
                                                <input id="drop-remove" class="magic-checkbox" type="checkbox">
                                                <label for="drop-remove">Remove after drop</label>
                                            </div>
                                            <hr class="bord-no">
                                            <p class="text-muted text-sm text-uppercase">Sample Events</p>
                                            <div class="fc-event" data-class="warning">All Day Event</div>
                                            <div class="fc-event" data-class="success">Meeting</div>
                                            <div class="fc-event" data-class="mint">Birthday Party</div>
                                            <div class="fc-event" data-class="purple">Happy Hour</div>
                                            <div class="fc-event">Lunch</div>
                                        </div>
                                        <!-- ============================================ -->
                                    </div>
                                    <div class="fluid">
                                        <!-- Calendar placeholder-->
                                        <!-- ============================================ -->
                                        <div id='demo-calendar'></div>
                                        <!-- ============================================ -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                </div>
                <!--===================================================-->
                <!--End page content-->

</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->