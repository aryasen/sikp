<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_Nilai extends CI_Model {

	function tampil_data_nilai_all(){
		$this->db->select('form_nilai.*, mahasiswa.nim, mahasiswa.fullname, program_studi.nama_program_studi');
		$this->db->from('form_nilai');
		$this->db->join('mahasiswa','mahasiswa.uid_mahasiswa=form_nilai.uid_mahasiswa');
		$this->db->join('program_studi','program_studi.uid_program_studi=mahasiswa.uid_program_studi');
		return $this->db->get()->result_array();
	}


	function tampil_data_nilai_dosena(){
		$this->db->select('form_nilai.nilai_dosena, form_nilai.uidform_nilai, mahasiswa.uid_mahasiswa, mahasiswa.nim, mahasiswa.fullname, mahasiswa.kelas, program_studi.nama_program_studi');
		$this->db->from('form_nilai');
		$this->db->join('mahasiswa','mahasiswa.uid_mahasiswa=form_nilai.uid_mahasiswa');
		$this->db->join('program_studi','program_studi.uid_program_studi=mahasiswa.uid_program_studi');
		return $this->db->get()->result_array();
	}
	function tampil_data_nilai_dosena_by_kelas($kelas){
		$this->db->select('form_nilai.nilai_dosena, form_nilai.uidform_nilai, mahasiswa.uid_mahasiswa, mahasiswa.nim, mahasiswa.fullname, mahasiswa.kelas, program_studi.nama_program_studi');
		$this->db->from('form_nilai');
		$this->db->join('mahasiswa','mahasiswa.uid_mahasiswa=form_nilai.uid_mahasiswa');
		$this->db->join('program_studi','program_studi.uid_program_studi=mahasiswa.uid_program_studi');
		$this->db->where('mahasiswa.kelas', $kelas);
		return $this->db->get()->result_array();
	}

	function tampil_data_nilai_dosena1($id_nilai){
		$this->db->select('form_nilai.nilai_dosena, form_nilai.uidform_nilai, mahasiswa.nim, mahasiswa.fullname, mahasiswa.kelas, program_studi.nama_program_studi');
		$this->db->from('form_nilai');
		$this->db->join('mahasiswa','mahasiswa.uid_mahasiswa=form_nilai.uid_mahasiswa');
		$this->db->join('program_studi','program_studi.uid_program_studi=mahasiswa.uid_program_studi');
		$this->db->where('uidform_nilai',$id_nilai);
		return $this->db->get()->row_array();
	}


	function tampil_data_nilai_dosenp(){
		$this->db->select('form_nilai.nilai_dosenp, mahasiswa.nim, mahasiswa.fullname, program_studi.nama_program_studi');
		$this->db->from('form_nilai');
		$this->db->join('mahasiswa','mahasiswa.uid_mahasiswa=form_nilai.uid_mahasiswa');
		$this->db->join('program_studi','program_studi.uid_program_studi=mahasiswa.uid_program_studi');
		return $this->db->get()->result_array();
	}

	function tampil_data_nilai_plap(){
		$this->db->select('form_nilai.nilai_plap, mahasiswa.nim, mahasiswa.fullname, program_studi.nama_program_studi');
		$this->db->from('form_nilai');
		$this->db->join('mahasiswa','mahasiswa.uid_mahasiswa=form_nilai.uid_mahasiswa');
		$this->db->join('program_studi','program_studi.uid_program_studi=mahasiswa.uid_program_studi');
		return $this->db->get()->result_array();
	}




 	function __construct(){
 		parent::__construct();
 	}

	function input_data($data,$table){
		$this->db->insert($table,$data);
	}

	function edit_data($table,$where){		
		return $this->db->get_where($table,$where);
	}

	public function update_data($table,$data,$where){
		return $this->db->update($table,$data,$where);
	}

}