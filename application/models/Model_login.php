<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Model_login extends CI_Model {

		public function cek_user($username,$password) {
			$sql = " 
			SELECT *
				FROM (
					SELECT 
						nim as username,
						password as password,
						'mahasiswa' as status
					FROM mahasiswa
					UNION
					SELECT
						nip as username,
						password as password,
						'dosen_akademik' as status
					FROM dosen_akademik
					UNION
					SELECT
						nip as username,
						password as password,
						'dosen_penguji' as status
					FROM dosen_penguji
					UNION
					SELECT
						username as username,
						password as password,
						'pemb_lap' as status
					FROM pbb_lapangan
					UNION
					SELECT
						username as username,
						password as password,
						'laa' as status
					FROM laa
					UNION
					SELECT
						nip as username,
						password as password,
						'monitoring' as status
					FROM monitoring
				) AS users
			WHERE username='".$username."' AND password='".$password."'";
			$query = $this->db->query($sql)->row_array();
			return $query;
		}

		public function get_data_user($username,$status) {
			if($status == 'mahasiswa') {
				$sql = "SELECT mahasiswa.* , program_studi.nama_program_studi FROM mahasiswa INNER JOIN program_studi ON mahasiswa.uid_program_studi=program_studi.uid_program_studi WHERE nim='".$username."'";
			}else
			if($status == 'dosen_akademik') {
				$sql = "SELECT nip,fullname,kelas FROM dosen_akademik WHERE nip='".$username."'";
			}else
			if($status == 'dosen_penguji') {
				$sql = "SELECT nip,fullname FROM dosen_penguji WHERE nip='".$username."'";
			}else
			if($status == 'pemb_lap') {
				$sql = "SELECT username,fullname,email,notel FROM pbb_lapangan WHERE username='".$username."'";
			}else
			if($status == 'laa') {
				$sql = "SELECT username,fullname FROM laa WHERE username='".$username."'";
			}else
			if($status == 'monitoring') {
				$sql = "SELECT nip,fullname FROM monitoring WHERE nip='".$username."'";
			}else{
				$sql = "";
			}
			$query = $this->db->query($sql)->row_array();
			return $query;
		}
	}