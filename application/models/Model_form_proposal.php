<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_form_proposal extends CI_Model {


	function tampil_data_proposal($id_proposal){
		$this->db->select('table_form_proposal.*, mahasiswa.nim, mahasiswa.fullname, mahasiswa.no_kontak, program_studi.nama_program_studi');
		$this->db->from('table_form_proposal');
		$this->db->join('mahasiswa','mahasiswa.uid_mahasiswa=table_form_proposal.uid_mahasiswa');
		$this->db->join('program_studi','program_studi.uid_program_studi=mahasiswa.uid_program_studi');
		$this->db->where('uidform_proposal',$id_proposal);
		return $this->db->get()->row_array();
	}

	function tampil_data_proposalall(){
		$this->db->select('table_form_proposal.*, mahasiswa.nim, mahasiswa.fullname, mahasiswa.kelas, program_studi.nama_program_studi');
		$this->db->from('table_form_proposal');
		$this->db->join('mahasiswa','mahasiswa.uid_mahasiswa=table_form_proposal.uid_mahasiswa');
		$this->db->join('program_studi','program_studi.uid_program_studi=mahasiswa.uid_program_studi');
		return $this->db->get()->result_array();
	}

	function tampil_data_proposal_kelas($kelas){
		$this->db->select('table_form_proposal.*, mahasiswa.nim, mahasiswa.fullname, mahasiswa.kelas, program_studi.nama_program_studi');
		$this->db->from('table_form_proposal');
		$this->db->join('mahasiswa','mahasiswa.uid_mahasiswa=table_form_proposal.uid_mahasiswa');
		$this->db->join('program_studi','program_studi.uid_program_studi=mahasiswa.uid_program_studi');
		$this->db->where('mahasiswa.kelas', $kelas);
		return $this->db->get()->result_array();
	}

	function tampil_data_diterima(){
		$this->db->select('table_form_proposal.*, mahasiswa.nim, mahasiswa.fullname, program_studi.nama_program_studi');
		$this->db->from('table_form_proposal');
		$this->db->join('mahasiswa','mahasiswa.uid_mahasiswa=table_form_proposal.uid_mahasiswa');
		$this->db->join('program_studi','program_studi.uid_program_studi=mahasiswa.uid_program_studi');
		$this->db->where('table_form_proposal.konfirmasi_dosena','Diterima');
		return $this->db->get()->result_array();
	}

 	function __construct(){
 		parent::__construct();
 	}

	function input_data($data,$table){
		$this->db->insert($table,$data);
	}

	public function update_data($table,$data,$where){
		return $this->db->update($table,$data,$where);
	}

}