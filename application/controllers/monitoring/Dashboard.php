<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		if ($this->session->userdata('nip')=="") 
		{
			redirect('login');
		}
	}

	public function index()
	{
		$this->load->view('monitoring/template/header.php');
		$this->load->view('dashboard.php');
		$this->load->view('monitoring/template/footer.php');
		

	}
}
