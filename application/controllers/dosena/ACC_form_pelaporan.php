<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ACC_form_pelaporan extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		if ($this->session->userdata('nip')=="") 
		{
			redirect('login');
		}
	}

	public function index()
	{	
		$data['tb']=$this->model_form_proposal->tampil_data_proposal_kelas($_SESSION['kelas']);
		$this->load->view('dosen_akademik/template/header.php');
		$this->load->view('dosen_akademik/ACC_form_pelaporan.php',$data);
		$this->load->view('dosen_akademik/template/footer.php');
	}

	public function get_dataProposal($id_proposal){
		$data = $this->model_form_proposal->tampil_data_proposal($id_proposal);
		echo json_encode($data);
	}



	public function acc($id_proposal){
		if($this->model_form_proposal->update_data('table_form_proposal',array('konfirmasi_dosena' => "Diterima"),array('uidform_proposal' => $id_proposal))){

		} else {

		}
		redirect('dosena/ACC_form_pelaporan');
	}

	public function tolak($id_proposal){
		if($this->model_form_proposal->update_data('table_form_proposal',array('konfirmasi_dosena' => "Ditolak"),array('uidform_proposal' => $id_proposal))){

		} else {

		}
		redirect('dosena/ACC_form_pelaporan');
	}

}
