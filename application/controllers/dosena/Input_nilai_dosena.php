<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Input_nilai_dosena extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('Model_Nilai');
		$this->load->helper('url');
		if ($this->session->userdata('nip')=="") 
		{
			redirect('login');
		}
	}	

	public function index()
	{
		$data['tb']=$this->Model_Nilai->tampil_data_nilai_dosena_by_kelas($_SESSION['kelas']);

		$this->load->view('dosen_akademik/template/header.php');
		$this->load->view('dosen_akademik/input_nilai.php',$data);
		$this->load->view('dosen_akademik/template/footer.php');
	}


	// public function get_datanilai($id_nilai){
	// 	$data = $this->model_form_proposal->tampil_data_nilai_dosena($id_nilai);
	// 	echo json_encode($data);
	// }

	public function get_nilai_a($id_nilai)
	{
		$data=$this->Model_Nilai->tampil_data_nilai_dosena1($id_nilai);
		echo json_encode($data);
	} 

	public function idata_nilai($id_nilai)
	{	

	    $data = array(
	        'nilai_dosena' => $_POST['nilai_a']
	    );

	    $this->Model_Nilai->update_data('form_nilai',$data,array('uid_mahasiswa' => $_POST['uid_mhs']));
		redirect('dosena/Dashboard');
	}
}
