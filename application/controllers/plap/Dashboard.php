<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		if ($this->session->userdata('username')=="") 
		{
			redirect('login');
		}
	}

	public function index()
	{
		$this->load->view('pemb_lapangan/template/header.php');
		$this->load->view('dashboard.php');
		$this->load->view('pemb_lapangan/template/footer.php');
	}
}
