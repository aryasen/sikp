<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('status')=='laa') {
			redirect('laa/Dashboard');
		}
		elseif ($this->session->userdata('status')=='mahasiswa') {
			redirect('mahasiswa/Dashboard');	
		}
		elseif ($this->session->userdata('status')=='dosena') {
			redirect('dosena/Dashboard');	
		}
		elseif ($this->session->userdata('status')=='dosenp') {
			redirect('dosenp/Dashboard');	
		}
		elseif ($this->session->userdata('status')=='plap') {
			redirect('plap/Dashboard');	
		}
		elseif ($this->session->userdata('status')=='monitoring') {
			redirect('monitoring/Dashboard');	
		}
	}

	public function index()
	{

		$this->load->view('login');
	}

	public function cek_login()
	{
		$data = array(
				'username' => $this->input->post('username', TRUE),
				'password' => $this->input->post('password', TRUE)
			);
		$hasil = $this->model_login->cek_user($data['username'],$data['password']);
		if ($hasil) {
			// foreach ($hasil->result() as $sess) {
			// 	// $sess_data['logged_in'] = 'Sudah Loggin';
			// 	// $sess_data['userid'] = $sess->userid;
			// 	// $sess_data['username'] = $sess->username;
			// 	// $sess_data['status'] = $sess->status;
			// 	$sess_data=array(
			// 		'status' => $sess->status,
			// 		'userid' => $sess->userid,
			// 		'username' => $sess->username,
			// 		'fullname' => $sess->fullname,
			// 	);

			// 	$this->session->set_userdata($sess_data);
			// }
			// if ($this->session->userdata('status')=='laa') {
			// 	redirect('laa/Dashboard');
			// }
			// elseif ($this->session->userdata('status')=='mahasiswa') {
			// 	redirect('mahasiswa/Dashboard');	
			// }
			// elseif ($this->session->userdata('status')=='dosena') {
			// 	redirect('dosena/Dashboard');	
			// }
			// elseif ($this->session->userdata('status')=='dosenp') {
			// 	redirect('dosenp/Dashboard');	
			// }
			// elseif ($this->session->userdata('status')=='plap') {
			// 	redirect('plap/Dashboard');	
			// }
			$data = $this->model_login->get_data_user($hasil['username'],$hasil['status']);
			$data['status'] = $hasil['status'];


			$this->session->set_userdata($data);

			if ($this->session->userdata('status')=='laa') {
				redirect('laa/Dashboard');
			}
			elseif ($this->session->userdata('status')=='mahasiswa') {
				redirect('mahasiswa/Dashboard');	
			}
			elseif ($this->session->userdata('status')=='dosen_akademik') {
				redirect('dosena/Dashboard');	
			}
			elseif ($this->session->userdata('status')=='dosen_penguji') {
				redirect('dosenp/Dashboard');	
			}
			elseif ($this->session->userdata('status')=='pemb_lap'){
				redirect('plap/Dashboard');	
			}
			elseif ($this->session->userdata('status')=='monitoring'){
				redirect('monitoring/Dashboard');	
			}
		}
		else {
			echo "<script>alert('Gagal login: Cek username, password!');history.go(-1);</script>";
		}
	}
}
