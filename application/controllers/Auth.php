<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function index() {
		$this->load->view('login');
	}

	public function cek_login() {
		$data = array('username' => $this->input->post('username', TRUE),
						'password' => md5($this->input->post('password', TRUE))
			);
		$this->load->model('model_user'); // load model_user
		$hasil = $this->model_user->cek_user($data);
		if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
				$sess_data['logged_in'] = 'Sudah Loggin';
				$sess_data['userid'] = $sess->userid;
				$sess_data['username'] = $sess->username;
				$sess_data['status'] = $sess->status;
				$this->session->set_userdata($sess_data);
			}
			if ($this->session->userdata('status')=='laa') {
				redirect('laa/Dashboard');
			}
			elseif ($this->session->userdata('status')=='mahasiswa') {
				redirect('mahasiswa/Dashboard');	
			}
			elseif ($this->session->userdata('status')=='dosena') {
				redirect('dosena/Dashboard');	
			}
			elseif ($this->session->userdata('status')=='dosenp') {
				redirect('dosenp/Dashboard');	
			}
			elseif ($this->session->userdata('status')=='plap') {
				redirect('plap/Dashboard');	
			}

		}
		else {
			echo "<script>alert('Gagal login: Cek username, password!');history.go(-1);</script>";
		}
	}

}

?>