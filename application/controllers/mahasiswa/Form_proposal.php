<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_proposal extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('model_form_proposal');
		$this->load->helper('url');
		if ($this->session->userdata('nim')=="") 
		{
			redirect('login');
		}
	}	

	public function index()
	{
		$this->load->view('mahasiswa/template/header.php');
		$this->load->view('mahasiswa/form_proposal.php');
		$this->load->view('mahasiswa/template/footer.php');
	}


	public function idata_fproposal()
	{	

	    $data = array(
	    	'uid_mahasiswa' => $_SESSION['uid_mahasiswa'],
	        'email' => $_POST['email'],
	        'pengajuanke' => $_POST['pengajuanke'],
	        'alasan_pengajuan_ulang' => $_POST['apu'],
	        'nama_lngp_instansi' => $_POST['nli'],
	        'alamat_lngp_instansi' => $_POST['ali'],
	        'kota' => $_POST['kota'],
	        'kodepos' => $_POST['kodepos'],
	        'ditujukan_kepada_fullname' => $_POST['fullname'],
	        'devisi_bagian' => $_POST['devb'],
	        'posisi_jabatan' => $_POST['posj'],
	        'notlp_instansi' => $_POST['notlp_i'],
	        'email_instansi' => $_POST['email_i'],
	        'contact_person_instansi' => $_POST['cpi'],
	        'tgl_mulai' => $_POST['startkp'],
	        'tgl_selesai' => $_POST['endkp'],
	        'BAB1' => $_POST['BAB1'],
	        'BAB2' => $_POST['BAB2'],
	    );
	   $this->model_form_proposal->input_data($data,'table_form_proposal');
		redirect('mahasiswa/Dashboard');
	}
}
