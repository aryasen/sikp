<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelaporan_KP extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		if ($this->session->userdata('nim')=="") 
		{
			redirect('login');
		}
	}

	public function index()
	{
		$this->load->view('mahasiswa/template/header.php');
		$this->load->view('mahasiswa/pelaporan_KP.php');
		$this->load->view('mahasiswa/template/footer.php');
		

	}
}
