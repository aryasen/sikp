<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class di_tolak extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		if ($this->session->userdata('nim')=="") 
		{
			redirect('login');
		}
	}

	public function index()
	{	
		$data['tb']=$this->model_form_proposal->tampil_data_proposalall();
		$this->load->view('mahasiswa/template/header.php');
		$this->load->view('mahasiswa/di_tolak.php',$data);
		$this->load->view('mahasiswa/template/footer.php');
	}

	public function get_dataProposal($id_proposal){
		$data = $this->model_form_proposal->tampil_data_proposal($id_proposal);
		echo json_encode($data);
	}

}
