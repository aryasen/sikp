<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_survei extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		if ($this->session->userdata('nim')=="") 
		{
			redirect('login');
		}
	}

	public function index()
	{
		$this->load->view('mahasiswa/template/header.php');
		$this->load->view('mahasiswa/form_survei.php');
		$this->load->view('mahasiswa/template/footer.php');
		

	}
}
