<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class sudahdiACC_dosena extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		if ($this->session->userdata('username')=="") 
		{
			redirect('login');
		}
	}

	public function index()
	{	
		$data['tb']=$this->model_form_proposal->tampil_data_diterima();
		$this->load->view('LAA/template/header.php');
		$this->load->view('LAA/sudahdiACC_dosena.php',$data);
		$this->load->view('LAA/template/footer.php');
	}

	public function get_dataProposal($id_proposal){
		$data = $this->model_form_proposal->tampil_data_proposal($id_proposal);
		echo json_encode($data);
	}

}
