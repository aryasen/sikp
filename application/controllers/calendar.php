<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calendar extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		if ($this->session->userdata('status')=="") 	
		{
			redirect('login');	
		}
	}

	public function index()
	{
		if ($this->session->userdata('status')=='laa') {
			$this->load->view('laa/template/header');
			$this->load->view('calendar');
			$this->load->view('laa/template/footer');
		}
		elseif ($this->session->userdata('status')=='mahasiswa') {
			$this->load->view('mahasiswa/template/header');
			$this->load->view('calendar');
			$this->load->view('mahasiswa/template/footer');
		}
		
		elseif ($this->session->userdata('status')=='dosen_akademik') {
			$this->load->view('dosen_akademik/template/header');
			$this->load->view('calendar');
			$this->load->view('dosen_akademik/template/footer');
		}

		elseif ($this->session->userdata('status')=='dosen_penguji') {
			$this->load->view('dosen_penguji/template/header');
			$this->load->view('calendar');
			$this->load->view('dosen_penguji/template/footer');
		}

		elseif ($this->session->userdata('status')=='pemb_lap') {
			$this->load->view('pemb_lapangan/template/header');
			$this->load->view('calendar');
			$this->load->view('pmb_lapangan/template/footer');
		}

		elseif ($this->session->userdata('status')=='monitoring') {
			$this->load->view('monitoring/template/header');
			$this->load->view('calendar');
			$this->load->view('monitoring/template/footer');
		}
		
				
	}

}
