$('#form_nilai_mhs').on('show.bs.modal', function(e) {
    var id = $(e.relatedTarget).data('id');
    console.log('trigger');
    $.ajax({
        type: "GET",
        url: site_url+'/mahasiswa/di_tolak/get_dataProposal/'+id,
        dataType : "json",
        success: function(result){
            console.log(result);
            $('#tb_nim').html(result.nim);
            $('#tb_fullname').html(result.fullname);
            $('#tb_nama_program_studi').html(result.nama_program_studi);
            $('#tb_no_kontak').html(result.no_kontak);
            $('#tb_email').html(result.email);
            $('#tb_pengajuanke').html(result.pengajuanke);
            $('#tb_alasan_pengajuan_ulang').html(result.alasan_pengajuan_ulang);
            $('#tb_nama_lngp_instansi').html(result.nama_lngp_instansi);
            $('#tb_alamat_lngp_instansi').html(result.alamat_lngp_instansi);
            $('#tb_kota').html(result.kota);
            $('#tb_kodepos').html(result.kodepos);
            $('#tb_devisi_bagian').html(result.devisi_bagian);
            $('#tb_posisi_jabatan').html(result.posisi_jabatan);
            $('#tb_notlp_instansi').html(result.notlp_instansi);
            $('#tb_email_instansi').html(result.email_instansi);
            $('#tb_contact_person_instansi').html(result.contact_person_instansi);
            $('#tb_tgl_mulai').html(result.tgl_mulai);
            $('#tb_tgl_selesai').html(result.tgl_selesai);
            $('#tb_BAB1').html(result.BAB1);
            $('#tb_BAB2').html(result.BAB2);
            $('#btnAcc').attr("href",site_url+"/dosena/acc_form_pelaporan/acc/"+id);
            $('#btnTolak').attr("href",site_url+"/dosena/acc_form_pelaporan/tolak/"+id);
        }
    })
});

