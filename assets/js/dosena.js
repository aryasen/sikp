$('#form-proposal-modal').on('show.bs.modal', function(e) {
    var id = $(e.relatedTarget).data('id');
    console.log('trigger');
    $.ajax({
        type: "GET",
        url: site_url+'/dosena/ACC_form_pelaporan/get_dataProposal/'+id,
        dataType : "json",
        success: function(result){
            console.log(result);
            $('#tb_nim').html(result.nim);
            $('#tb_fullname').html(result.fullname);
            $('#tb_nama_program_studi').html(result.nama_program_studi);
            $('#tb_no_kontak').html(result.no_kontak);
            $('#tb_email').html(result.email);
            $('#tb_pengajuanke').html(result.pengajuanke);
            $('#tb_alasan_pengajuan_ulang').html(result.alasan_pengajuan_ulang);
            $('#tb_nama_lngp_instansi').html(result.nama_lngp_instansi);
            $('#tb_alamat_lngp_instansi').html(result.alamat_lngp_instansi);
            $('#tb_kota').html(result.kota);
            $('#tb_kodepos').html(result.kodepos);
            $('#tb_devisi_bagian').html(result.devisi_bagian);
            $('#tb_posisi_jabatan').html(result.posisi_jabatan);
            $('#tb_notlp_instansi').html(result.notlp_instansi);
            $('#tb_email_instansi').html(result.email_instansi);
            $('#tb_contact_person_instansi').html(result.contact_person_instansi);
            $('#tb_tgl_mulai').html(result.tgl_mulai);
            $('#tb_tgl_selesai').html(result.tgl_selesai);
            $('#tb_BAB1').html(result.BAB1);
            $('#tb_BAB2').html(result.BAB2);
            $('#btnAcc').attr("href",site_url+"/dosena/acc_form_pelaporan/acc/"+id);
            $('#btnTolak').attr("href",site_url+"/dosena/acc_form_pelaporan/tolak/"+id);
            if(result.konfirmasi_dosena == "Diterima" || result.konfirmasi_dosena == "Ditolak" ){
                $('a#btnAcc').hide();
                $('a#btnTolak').hide();
            } else {
                $('a#btnAcc').show();
                $('a#btnTolak').show();
            }
        }
    })
});
$('#form-proposal-modal-admin').on('show.bs.modal', function(e) {
    var id = $(e.relatedTarget).data('id');
    console.log('trigger');
    $.ajax({
        type: "GET",
        url: site_url+'/laa/sudahdiACC_dosena/get_dataProposal/'+id,
        dataType : "json",
        success: function(result){
            console.log(result);
            $('#tb_nim').html(result.nim);
            $('#tb_fullname').html(result.fullname);
            $('#tb_nama_program_studi').html(result.nama_program_studi);
            $('#tb_no_kontak').html(result.no_kontak);
            $('#tb_email').html(result.email);
            $('#tb_pengajuanke').html(result.pengajuanke);
            $('#tb_alasan_pengajuan_ulang').html(result.alasan_pengajuan_ulang);
            $('#tb_nama_lngp_instansi').html(result.nama_lngp_instansi);
            $('#tb_alamat_lngp_instansi').html(result.alamat_lngp_instansi);
            $('#tb_kota').html(result.kota);
            $('#tb_kodepos').html(result.kodepos);
            $('#tb_devisi_bagian').html(result.devisi_bagian);
            $('#tb_posisi_jabatan').html(result.posisi_jabatan);
            $('#tb_notlp_instansi').html(result.notlp_instansi);
            $('#tb_email_instansi').html(result.email_instansi);
            $('#tb_contact_person_instansi').html(result.contact_person_instansi);
            $('#tb_tgl_mulai').html(result.tgl_mulai);
            $('#tb_tgl_selesai').html(result.tgl_selesai);
            $('#tb_BAB1').html(result.BAB1);
            $('#tb_BAB2').html(result.BAB2);
            $('#btnAcc').attr("href",site_url+"/dosena/ACC_form_pelaporan/acc/"+id);
            $('#btnTolak').attr("href",site_url+"/dosena/ACC_form_pelaporan/tolak/"+id);
        }
    })
});
$('#form-nilaimodala').on('show.bs.modal', function(e) {
    var id = $(e.relatedTarget).data('id');
    var uid_mhs = $(e.relatedTarget).data('uid_mhs');
    $('#formeditnilaidosena').attr('action', site_url+'/dosena/Input_nilai_dosena/idata_nilai/'+id);
    console.log('trigger');
    $.ajax({
        type: "GET",
        url: site_url+'/dosena/Input_nilai_dosena/get_nilai_a/'+id,
        dataType : "json",
        success: function(result){
            console.log(result);
            $('#nimisimodal').html(result.nim);
            $('input#uid_mhs').val(uid_mhs);
            $('#namaisimodal').html(result.fullname); 
            $('input#nama').val(result.fullname); 
            $('#nilai_a').val(result.nilai_dosena);
        }
    })
});
